package com.halas;

import java.util.Arrays;
import java.util.List;

public class Student {
    private String name;
    private int age;
    private int course;
    private String university;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getCourse() {
        return course;
    }

    public void upCourse(int course) {
        this.course++;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public Integer[] getMarks(String marks) throws NullPointerException {
        Integer[] studentMarks = new Integer[marks.length()];
        int i=0;
        for (char mark : marks.toCharArray()) {
            studentMarks[i]=Integer.getInteger(mark + "");
            i++;
        }
        return studentMarks;
    }

    public static double divideNumbers(double first,double second){
        return first/second;
    }

    public List getDescription(String text){
        return Arrays.asList(text.split("!.?"));
    }
}
