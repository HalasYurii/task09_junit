package com.halas;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.runners.Parameterized.*;

@RunWith(Parameterized.class)
public class ParametrizedTest {

    // fields used together with @Parameter must be public
    @Parameterized.Parameter(0)
    public double m1;
    @Parameterized.Parameter(1)
    public double m2;
    @Parameterized.Parameter(2)
    public double result;


    // creates the test data
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] {
                { 6 , 3, 2.0 }, { 5, 2, 2.5 }, { 123, 3, 41 }
        };
        return Arrays.asList(data);
    }


    @Test
    public void testMultiplyException() {
        MyClass tester = new MyClass();
        assertEquals("Result", result, tester.multiply(m1, m2),0.0);
    }


    // class to be tested
    class MyClass {
        public double multiply(double i, double j) {
            return i/j;
        }
    }
}
