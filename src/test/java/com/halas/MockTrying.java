package com.halas;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class MockTrying {

    //create from Student mock
    @Test
    public void getDescriptionTest(){
        Student st = mock(Student.class);
        List answer = Collections.singletonList("Fine" + " Hope will be good programmer" + " I think he must trying more");
        when(st.getDescription("a")).thenReturn(answer);
        assertEquals(st.getDescription("a"),answer);
    }


    // demonstrates the return of multiple values
    @Test
    public void testMoreThanOneReturnValue()  {
        Iterator i= mock(Iterator.class);
        when(i.next()).thenReturn("Mockito").thenReturn("rocks").thenReturn("end");
        String result= i.next()+" "+i.next()+" "+i.next();
        //assert
        assertEquals("Mockito rocks end", result);
    }

    @Test
    //@Disabled
    public void testLinkedListSpyWrong() {
        List spy = mock(List.class);

        // this does not work
        // real method is called so spy.get(0)
        // throws IndexOutOfBoundsException (list is still empty)
        when(spy.get(0)).thenReturn("foo");

        assertEquals("foo", spy.get(0));
    }


    @Test
    public void testLinkedListSpyCorrect() {
        List spy = mock(List.class);

        // You have to use doReturn() for stubbing
        doReturn("Zero").when(spy).get(0);

        assertEquals("Zero", spy.get(0));
    }

    @Test
    public void verifyTest(){
        Student student = mock(Student.class);
        when(student.getMarks("")).thenReturn(new Integer[]{2,3});

        student.getMarks("");
        student.getAge();
        student.getAge();
        student.getAge();

        student.getCourse();
        student.getCourse();

        //check if getMarks called with parameter ""
        verify(student).getMarks(ArgumentMatchers.eq(""));

        //check if was called 3-times
        verify(student,times(3)).getAge();

        //check if was called 2-times
        verify(student,times(2)).getCourse();

        verify(student,never()).getDescription("never called");
        //check also look at parameters in functions
        verify(student,atLeastOnce()).getMarks("");
    }
}
